# Design and implement a python program to get a string made of the first 2 and
#  the last 2 chars from a given string. If the string length is less than 2
#  return instead the empty string.

def func():
    finalStr= str()
    inputStr= input('enter string')
    if (len(inputStr)<2):
        return ""
    finalStr=inputStr[0:2]+inputStr[len(inputStr)-2:]
    return finalStr


