# Design and implement a python program to generate and print 
# list except the first 5 elements, where the values are square 
# of numbers between 1 and 30(both included).

#brute force

squareList=list()
finalList=list()
for i in range (31):
    squareList.append(i*i)
inputList= [int(x) for x in input('enter nums').split()]
for num in inputList[0:5]:
    if (num in squareList):
        pass
    else:
        finalList.append(num)
print(finalList)
