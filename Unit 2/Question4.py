# Design and implement a python function that takes two lists and returns true if they have at least one common.
def func():
    firstList =[int(x) for x in input('enter first list nums').split()]
    secList =[int(x) for x in input('enter second list nums').split()]
    for n in firstList:
        if (n in secList):
            return True
    return False

